// Add your requirements
var restify = require('restify');
var builder = require('botbuilder');
var appInsights = require('applicationinsights');


// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.PORT || 3000, function()
{
   console.log('%s listening to %s', server.name, server.url);
});

// Create chat bot
var connector = new builder.ChatConnector
({ appId: '8fe62ff6-fb44-4782-a03c-b4450106f6b8', appPassword: 'yDwERr3Fp9VPb0ULW7LYeKj' });
var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());


// Call applicationinsights key here:

appInsights.setup('ca5a2667-85fb-4afd-a946-e6b478f6e1c3').start();

// Create bot dialogs
/*
bot.dialog('/', function (session) {
    session.send("Hello World");
});

*/

bot.recognizer({
    recognize:function(context, done){
        var intent = {score:0.0};
        if (context.message.text){
            switch (context.message.text.toLowerCase()){

                case'hi':
                case 'hello':
                case 'hey':
                intent={score:1.0,intent:'Hi'};
                break;

              case "hi, i need some help":
              case 'need':
              case 'i need help':
              case 'can you help me':
              case 'i need some information':
              case 'i need info':
              case 'hi i need some help':
              case 'hi i need help':
              case 'hey I need some help':
              intent ={score:1.0,intent:'Hi i need help'}
              break;

                case 'hey are you there?':
                case 'hey are you there':
                case 'anyone here':
                case 'hey are you here?':
                case 'are you here':
                intent={score:1.0,intent:'Are you here'};
                break;


                case 'whats your name':
                case "what's your name?":
                case 'what is your name':
                intent={score:1.0,intent:'Name'};
                break;

                case 'hi would i get warranty with my product if i buy from you':
                case 'would i get warranty':
                case 'do i get warranty':
                case 'warranty':
                case 'do we get warranty':
                case 'would we get warranty':
                case 'are the products that we buy covered under warranty?':
                case 'do your products come with warranty':
                intent = {score:1.0,intent:'would i get warranty'};
                break;

                case 'hi would i get warranty with my product if i buy from you guys':
                case 'are the products that you sell genuine':
                case 'how do i know that you would send only a genuine product to me':
                case 'are your products genuine':
                case 'are your products original':
                case 'are your products original?':
                case 'would i get original product':
                case 'would i get the original product':
                case 'do you sell original products':
                case 'do you sell original products?':
                intent = {score:1.0,intent:'Are the products that you sell genuine'};
                break;

                case'is the apple india warranty applicable on my purchase':
                case'apple india':
                case'do we get apple warranty':
                case 'does it come with apple warranty':
                intent={score:1.0,intent:'Apple India'};
                break;

                case'is the samsung india warranty applicable on my purchase?':
                case'samsung india':
                case'do we get samsung warranty':
                case'does it come with samsung warranty':
                intent={score:1.0,intent:'Samsung India'};
                break;

                case'is the canon India warranty applicable on my purchase?':
                case'do we get canon warranty':
                case'does it come with canon warranty':
                case'canon india':
                intent={score:1.0,intent:'Canon India'};
                break;

                case 'is the manfrotto warranty applicable on my purchase?':
                case'do we get manfrotto warranty':
                case'does it come with manfrotto warranty':
                case 'manfrotto':
                intent = {score:1.0,intent:'Manfrotto'};
                break;

                case'do you deliver':
                case'where do you ship':
                case'do you ship to':
                case'deliver':
                intent={score:1.0,intent:'Deliver'};
                break;

                case'do you deliver internationally':
                case 'can i get delivery out side of india':
                case 'do you deliver abroad':
                case 'can you deliver abroad':
                case 'do you do international delivery':
                case 'can you ship abroad':
                case 'can you ship internationally':
                case 'do you ship internationally':
                case'do you deliver internationally?':
                case 'can i get delivery out side of india?':
                case 'do you deliver abroad?':
                case 'can you deliver abroad?':
                case 'do you do international delivery?':
                case 'can you ship abroad?':
                case 'can you ship internationally?':
                intent={score:1.0,intent:'International'};
                break;

                case'how long do you take to deliver?':
                case'If i place an order today, how long would you take to deliver the product?':
                case'If i place an order today, by when would it be delivered?':
                case'what is your delivery period':
                case'how soon do you deliver':
                case 'by when would i get my order':
                case 'how long do you take to deliver':
                case 'how long does it take for delivery':
                intent={score:1.0,intent:'Delivery period'};
                break;

                case'do you charge for delivery?':
                case'hey how much do you charge for delivery?':
                case'do you charge for delivery':
                case'how much do you charge for delivery':
                case 'what are the delivery charges':
                intent={score:1.0,intent:'Delivery charges'};
                break;

                case'do you accept cod?':
                case'do you have cod?':
                case'can i pay at the time of delivery?':
                case'do you charge for cod':
                case'cash on delivery':
                case'cash on delivery charges':
                intent={score:1.0,intent:'Cash on delivery'};
                break;

                case'what payment method do you offer':
                case"what payment methods do you offer":
                case'how can i pay for my order':
                case'how can i pay':
                case'payment':
                intent={score:1.0,intent:'Payment'};
                break;

                case'do you offer emi?':
                case'do you offer emi':
                case'do you have emi':
                case'can i buy using emis?':
                case'can i buy products on emi?':
                case 'can i pay by emi':
                case 'can i pay on emi':
                case'are emis available':
                case'emi':
                intent={score:1.0,intent:'Emi'};
                break;

                case 'help':
                case 'can i talk to someone':
                case 'how can i contact you':
                case 'how to contact you':
                case 'how do i get in touch with you':
                intent = { score:1.0,intent:'Help'};
                break;

                case 'whats your email id':
                case "what's your email id":
                case "what is your email id?":
                case "what is your email id":
                case "what's your email id?":
                case "whats your email id?":
                case "whats your email":
                case"what's your email?":
                case "what is your email":
                case"is there an email where i can email you":
                intent={score:1.0,intent:'Email'};
                break;

                case'what all products do you offer?':
                case'show me products':
                case'show product':
                case'show products':
                case'product categories':
                case'products':
                case'product':
                intent = {score:1.0,intent:'Product categories'};
                break;

                case 'camera category':
                case 'cameras and accessories':
                intent= {score:1.0,intent:'Cameracategory'};
                break;

                case 'audio & music':
                intent={score:1.0,intent:'Audio & music'};
                break;

                case 'computer category':
                case 'please show all products you offer':
                intent={score:1.0,intent:'Computer category'};
                break;

                case 'mobiles':
                case 'mobile':
                case'i am interested in buying a new smartphone':
                intent={score:1.0,intent:'Mobiles'};
                break;
                //Button for Android topseller:
                case 'android button':
                case 'android phones':
                intent={score:1.0,intent:'Android button'};
                break;

                // Android crausel:
                case 'android categories':
                case'show me the top selling phones':
                intent={score:1.0,intent:'Android categories'};
                break;

                //Apple button:
                case'apple button':
                case'apple iphones':
                intent={score:1.0,intent:'Apple button'};
                break;

                //Apple Iphone crausel:
                case 'apple categories':
                case'show me the top selling iphones':
                intent={score:1.0,intent:'Apple categories'};
                break;

                /*
                case'canon':
                intent={score:1.0,intent:'Canon'};
                break;
                */

                case 'get lost':
                case 'fuck you':
                case 'shut up':
                case 'you are crazy':
                case 'you cannot help me':
                case 'idiot':
                case 'stupid':
                case 'chal bhaag':
                case 'fuck off':
                case 'you are a fool':
                case 'asshole':
                case'f o':
                case 'fo':
                intent={score:1.0,intent:'Get lost'};
                break;

                case 'will you marry me':
                case 'how tall are you':
                case 'what are you wearing':
                case 'send me your picture':
                case 'would you go out with me':
                case 'marry me':
                case "let's date":
                case 'want to have coffee':
                case 'coffee?':
                case 'want to have tea':
                case 'tea?':
                case 'want to have juice':
                case 'juice?':
                case "let's run away":
                case 'i want to date you':
                intent={score:1.0,intent:'Will you marry me'};
                break;


                default:
                intent={score:1.0,intent:'syntax error'};
                break
            }
        }
        done(null, intent);
    }
});

// hi dialog:

/*
bot.dialog('hi',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog("Hi there, Welcome to Gizmofashion.com, India's leading brand for electronic products & accessories! How can I help you?");
  },2000);
}).triggerAction({matches:'Hi'});
*/
bot.dialog('hi',function(session){
  session.send("Hi there, welcome to Gizmofashion.com, India's leading brand for electronic products & accessories! Please select from the following options so that I can assist you.");
  var msg = new builder.Message(session)
  .attachments([{
    contentType:"application/vnd.microsoft.card.hero",
    content:{
    //  text:"Hi there, Welcome to Gizmofashion.com, India's leading brand for electronic products & accessories! How can I help you?",
      buttons:[
        {
          "type":"imBack",
          "title":"Product Related",
          "value":"What all products do you offer?"
        },
        {
          "type":"openUrl",
          "title":"Returns Related",
          "value":"http://www.gizmofashion.com/return-exchange"
        },
        {
          "type":"openUrl",
          "title":"Payment Related",
          "value":"http://www.gizmofashion.com/payments"
        },

        {
          "type":"imBack",
          "title":"Warranty Related",
          "value":"Are the products that we buy covered under warranty?"
        },
        {
          "type":"openUrl",
          "title":"Shipment Related",
          "value":"http://www.gizmofashion.com/shipping"
        },

      ]
    }
  }]);
  session.endDialog(msg);
}).triggerAction({matches:'Hi'});

bot.dialog('hi i need help',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Hi there, sure please let me know how can I assist?');
  },2000);
}).triggerAction({matches:'Hi i need help'});

bot.dialog('are you here',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Hi there, welcome to Gizmofashion! How can I help you?');
  },2000);
}).triggerAction({matches:'Are you here'});

bot.dialog('name',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('I am Anya and I will be assisting you today.');
  },2000);
}).triggerAction({matches:'Name'});

bot.dialog('would i get warranty',function(session){
  session.sendTyping();
  setTimeout(function(){
   session.endDialog('Absolutely! As official partners with top global brands like Apple, Canon & others you would ALWAYS enjoy 100% genuine products & India as well as global (whereever applicable) manufacturer warranty.');
 },2000);
}).triggerAction({matches:'would i get warranty'});

bot.dialog('Are the products that you sell genuine',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Absolutely! As official partners with top global brands like Apple, Canon & others you would ALWAYS enjoy 100% genuine products & India as well as global (whereever applicable) and manufacturer warranty when you buy from us.');
  },2000);
}).triggerAction({matches:'Are the products that you sell genuine'});

bot.dialog('Apple india',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Absolutely! As official partners for Apple India, all our products are 100% genuine and would come with Apple India warranty. So feel free to buy with complete peace of mind!');
  },2000);
}).triggerAction({matches:'Apple India'});

bot.dialog('samsung india',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Absolutely! As official partners for Samsung, all our products are 100% genuine and would come with Samsung India warranty. So feel free to buy with complete peace of mind!');
  },2000);
}).triggerAction({matches:'Samsung India'});

bot.dialog('canon india',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Absolutely! As official partners for Canon India, all our products are 100% genuine and would come with Canon India warranty. So feel free to buy with complete peace of mind!');
  },2000);
}).triggerAction({matches:'Canon India'});

bot.dialog('manfrotto',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Absolutely! As official partners for Manfrotto, all our products are 100% genuine and would come with Manfrotto warranty. So feel free to buy with complete peace of mind!');
  },2000);
}).triggerAction({matches:'Manfrotto'});

bot.dialog('deliver',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('When you shop with us, you enjoy FREE delivery across India. For confirmation of delivery to your pin code, please email us your pin code at customerservice@gizmofashion.com and we would revert back asap! <br/> For more shipment related information, please visit: http://www.gizmofashion.com/shipping');
  },2000);
}).triggerAction({matches:'Deliver'});

bot.dialog('international',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Currently we are only shipping within India and not internationally.<br/>For more shipment related information, please visit: http://www.gizmofashion.com/shipping');
  },2000);
}).triggerAction({matches:'International'});

bot.dialog('How long do you take to deliver',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Typically we deliver within 48 hours post shipment to most of the cities across India for FREE. For confirmation of delivery to your pin code, please email us your pin code at customerservice@gizmofashion.com and we would revert back asap! <br/> For more shipment related information, please visit: http://www.gizmofashion.com/shipping');
  },2000);
}).triggerAction({matches:'Delivery period'});

bot.dialog('do you charge for delivery',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('When you shop with us, you enjoy FREE delivery across India! So feel free to buy with complete peace of mind.');
  },2000);
}).triggerAction({matches:'Delivery charges'});

bot.dialog('cash on delivery',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('No, currently we do not charge for COD. For more payments related information, please visit: http://www.gizmofashion.com/payments');
  },2000);
}).triggerAction({matches:'Cash on delivery'});

bot.dialog('payment',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('We offer you the convenience of paying via your preferred mode of payment including Credit/Debit Cards, Netbankking, IMPS, UPI, ATM Cards, eWallets & more. For more payments related information, please visit: http://www.gizmofashion.com/payments');
  },2000);
}).triggerAction({matches:'Payment'});

bot.dialog('emi',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog('Absolutely! We offer EMI’s from banks like ICICI, Axis, IndusInd, HSBC, Kotak and American Express. For more information, please visit http://www.gizmofashion.com/emi-policy');
  },2000);
}).triggerAction({matches:'Emi'});

//Add a dialog to trigger action bound to help intent:
bot.dialog('help Dialog',function(session){
    session.sendTyping();
    setTimeout(function(){
        session.endDialog("Our Customer Happiness Specialist will be delighted to help you. Please call us at +91-9555-944-966 anytime between 10 AM till 7 PM, Monday to Friday and between 10 AM till 1 PM on Saturday. Or simply email us your query at customerservice@gizmofashion.com and we would get back to you within 24 hours. ");
    },2000);
}).triggerAction({matches:'Help'});

bot.dialog('email',function(session){
    session.sendTyping();
    setTimeout(function(){
        session.endDialog("Please email us at customerservice@gizmofashion.com and your friendly Customer Happiness Specialist would get back to you. Or just call us at +91-9555-944-966 anytime between 10 AM till 7 PM, Monday to Friday and between 10 AM till 1 PM on Saturday.");
    },2000);
}).triggerAction({matches:'Email'});

bot.dialog('get lost',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog("Aww. That's not really nice, is it? I can only talk to you if you behave nicely!");
  },2000);
}).triggerAction({matches:'Get lost'});

bot.dialog('will you marry me',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog("Oh my my. You really are a colorful person, aren't you!? We would love to help but you need to ask only relevant questions.");
  },2000);
}).triggerAction({matches:'Will you marry me'});

// For default Answer:

bot.dialog('syntax error',function(session){
  session.sendTyping();
  setTimeout(function(){
    session.endDialog("I am sorry but I cannot understand your message. Please rephrase your message or type Help.");
  },2000);
}).triggerAction({matches:'syntax error'});


//Add apple:

/*
bot.dialog('mackbook',function(session){
        session.send("If you Order Today you will get 20% discount on this product");
        var msg = new builder.Message(session)
        .attachments([{
            contentType:"image/jpeg",
            contentUrl:"https://www.gizmofashion.com/media/catalog/product/a/p/apple_macbook_air_back_view_gizmofashion.com_4.jpg"
        }]);
    session.endDialog(msg);
}).triggerAction({matches:'Mackbook'});

*/


//Add Canon:
/*
bot.dialog('canon',function(session){
  session.sendTyping();
  setTimeout(function(){
    var msg= new builder.Message(session)
    .attachments([{
      contentType:"image/jpeg",
      contentUrl:"https://www.gizmofashion.com/media/catalog/product/c/a/canon_ef_135mm_f2l_usm_lens_2520a005aa_1.jpg"
    }]);
    session.endDialog(msg);
  },2000);
}).triggerAction({matches:'Canon'});
*/
/*
bot.dialog('Canon',function(session){
  session.send("");
  var msg= new builder.Message(session)
  .attachments([{
    contentType:"image/jpeg",
    contentUrl:"https://www.gizmofashion.com/media/catalog/product/c/a/canon_ef_135mm_f2l_usm_lens_2520a005aa_1.jpg"
  }]);
  session.endDialog(msg);
}).triggerAction({matches:'Canon'});
*/

// Android TOp Sellers
bot.dialog('android button',function(session){
  session.sendTyping();
  setTimeout(function(){
     var msg = new builder.Message(session)
      .attachments([{
        contentType:"application/vnd.microsoft.card.hero",
        content:{
          text:"Please select an option:",
          buttons:[
            {
              "type":"imBack",
              "title":"Top Sellers",
              "value":"Show me the top selling Phones"
            },
            {
              "type":"openUrl",
              "title":"All Phones",
              "value":"http://www.gizmofashion.com/mobiles/smartphones/android-phones"
            }
          ]
        }
      }]);
      session.endDialog(msg);
  },2000);
}).triggerAction({matches:'Android button'});



// select iPhone button Sellers
bot.dialog('apple button',function(session){
  session.sendTyping();
  setTimeout(function(){
     var msg = new builder.Message(session)
      .attachments([{
        contentType:"application/vnd.microsoft.card.hero",
        content:{
          text:"Please select an option:",
          buttons:[
            {
              "type":"imBack",
              "title":"Top Sellers",
              "value":"Show me the top selling iPhones"
            },
            {
              "type":"openUrl",
              "title":"All iPhones",
              "value":"http://www.gizmofashion.com/mobiles/smartphones/apple-iphones"
            }
          ]
        }
      }]);
      session.endDialog(msg);
  },2000);
}).triggerAction({matches:'Apple button'});

//Experiment button code:
bot.dialog('show me products',function(session){
  session.send("Please select the category that you are interested in:");
        var msg = new builder.Message(session)

        .attachments([{
            contentType:"application/vnd.microsoft.card.hero",
            content:{
            //  text:"what your are looking for ?",
              buttons:[
              {
                "type":"imBack",
                "title":"Mobiles",
                "value":'I am interested in buying a new smartphone'
              },
              {
                "type":"imBack",
                "title":"Cameras",
                "value":"Cameras and Accessories"
              },
              {
                "type":"imBack",
                "title":"Audio & Music",
                "value":"Audio & Music"
              },
              {
                "type":"openUrl",
                "title":"Air Conditioners",
                "value":"http://www.gizmofashion.com/air-conditioners"
              },
              {
                "type":"imBack",
                "title":"Computer & Tablets",
                "value":"Please show all products you offer"
              }

            ]
            }
        }]);
    session.endDialog(msg);
}).triggerAction({matches:'Product categories'});

//Computer and tablets:
bot.dialog('Computer category',function(session){
  session.send("Please select from the following options:");
  var msg = new builder.Message(session)

    .attachments([{
      contentType:"application/vnd.microsoft.card.hero",
      content:{
      //  text:"Please select from the following options:",
        buttons:[
          {
            "type":"openUrl",
            "title":"Tablets",
            "value":"http://www.gizmofashion.com/computers-tablets/tablets-e-readers"
          },
          {
            "type":"openUrl",
            "title":"Computers/Desktops",
            "value":"http://www.gizmofashion.com/computers-tablets/computers"
          }

        ]
      }
    }]);
    session.endDialog(msg);
}).triggerAction({matches:'Computer category'});


// cameras category
bot.dialog('Camera category',function(session){
  session.send("Please select the category that you are interested in:");
var msg = new builder.Message(session)

  .attachments([{
    contentType:"application/vnd.microsoft.card.hero",
    content:{
      //text:"Select category ",
      buttons:[
        {
          "type":"openUrl",
          "title":"Lenses",
          "value":"http://www.gizmofashion.com/cameras/lenses"
        },
        {
          "type":"openUrl",
          "title":"Camera Bags",
          "value":"http://www.gizmofashion.com/cameras/accessories/camera-bags"
        },
        {
          "type":"openUrl",
          "title":"Monopods and Tripods",
          "value":"http://www.gizmofashion.com/cameras/accessories/monopods-and-tripods"
        },
        {
          "type":"openUrl",
          "title":"Tripod Heads",
          "value":"http://www.gizmofashion.com/cameras/accessories/tripod-heads"
        },
        {
          "type":"openUrl",
          "title":"Plates & Clamps",
          "value":"http://www.gizmofashion.com/cameras/accessories/plates-clamps"
        }

      ]
    }
  }]);
  session.endDialog(msg);
}).triggerAction({matches:'Cameracategory'});

//Audio and music:
bot.dialog('audio & music',function(session){
session.send("Please select from the following options:");
  var msg = new builder.Message(session)

    .attachments([{
      contentType:"application/vnd.microsoft.card.hero",
      content:{
      //  text:"Select the Button",
        buttons:[
          {
            "type":"openUrl",
            "title":"Headphones",
            "value":"http://www.gizmofashion.com/audio-music/headphones"
          },
          {
            "type":"openUrl",
            "title":"Earphones",
            "value":"http://www.gizmofashion.com/audio-music/earphones/in-ear-earphones"
          },

          {
            "type":"openUrl",
            "title":"ProAudio",
            "value":"http://www.gizmofashion.com/audio-music/pro-audio"
          },
          {
            "type":"openUrl",
            "title":"Speakers",
            "value":"http://www.gizmofashion.com/audio-music/speakers"
          }
        ]
      }
    }]);
    session.endDialog(msg);
}).triggerAction({matches:'Audio & music'});


// List of Mobiles:
bot.dialog('mobiles',function(session){
  session.send("Please select the smartphone type that you are looking for");
  var msg = new builder.Message(session)
    .attachments([{
          contentType:"application/vnd.microsoft.card.thumbnail",

          content:{
          //  text:"which Mobile are you looking for ?",
            //Buttons = "cardButtons",
            buttons:[
                      {
                        "type":"imBack",
                        "title":"Apple iPhones",
                        "value":"Apple iPhones"
                      },
                      {

                        "type":"imBack",
                        "title":"Android Phones",
                        "value":"Android Phones"
                      }
          ]
          }
    }]);
    session.endDialog(msg);
}).triggerAction({matches:'Mobiles'});

//Top seller Android Crausel:
bot.dialog('android categories',function(session){
var msg= new builder.Message(session)
//  .textFormat(builder.textFormat.xml)
  .attachmentLayout(builder.AttachmentLayout.carousel)
  .attachments([

    new builder.HeroCard(session)
      .title("Samsung Galaxy S8")
      .subtitle("64GB Maple Gold")
      //.text("Rs 57,400")
      .images([
        builder.CardImage.create(session,"https://www.gizmofashion.com/media/bot/s/bsamsung_galaxy_s8_maple_gold_-_gizmofashion_5.jpg")
      ])
      .buttons([
       builder.CardAction.openUrl(session,'http://www.gizmofashion.com/samsung-galaxy-s8-64gb-maple-gold','Shop Now'),
        //builder.CardAction.imBack(session,"Back")
      ]),

    new builder.HeroCard(session)
      .title("Samsung Galaxy S7 Edge")
      .subtitle("32GB Gold Platinum")
      //.text("Rs 50,200 ")

  .images([
        builder.CardImage.create(session,'https://www.gizmofashion.com/media/bot/s/bsamsung_galaxy_s7_gold_platinum_front.jpg')

    ])
      .buttons([
         builder.CardAction.openUrl(session,'http://www.gizmofashion.com/samsung-galaxy-s7-edge-32gb-gold-platinum','Shop Now'),
        //  builder.CardAction.imBack(session,"select:100","Select")
      ]),


    new builder.HeroCard(session)
      .title("Samsung Galaxy S7")
      .subtitle("32GB Gold Platinum")
      //.text("Rs. 42,500")
      .images([
        builder.CardImage.create(session,"https://www.gizmofashion.com/media/bot/s/bsamsung_galaxy_s7_gold_platinum_front.jpg")
      ])
      .buttons([
       builder.CardAction.openUrl(session,'http://www.gizmofashion.com/samsung-galaxy-s7-32gb-gold-platinum','Shop Now'),
        //builder.CardAction.imBack(session,"Back")
      ]),
      new builder.HeroCard(session)
      .title("Samsung Galaxy A7")
      .subtitle("Galaxy A7 (2016) Black")
      //.text("Rs. 25,500")
        .images([
          builder.CardImage.create(session,"https://www.gizmofashion.com/media/bot/s/bsamsung_galaxy_a7_2016_black_front.jpg")
        ])
        .buttons([
         builder.CardAction.openUrl(session,'http://www.gizmofashion.com/samsung-galaxy-a7-2016-black','Shop Now'),
          //builder.CardAction.imBack(session,"Back")
        ]),
        new builder.HeroCard(session)
          .title("Samsung Galaxy S7")
          .subtitle("Edge 32GB Pink Gold")
          //.text("Rs. 50,200")
          .images([
            builder.CardImage.create(session,"https://www.gizmofashion.com/media/bot/s/bsamsung_galaxy_s7_edge_pink_gold_front_-_gizmofashion.jpg")
          ])
          .buttons([
           builder.CardAction.openUrl(session,'http://www.gizmofashion.com/samsung-galaxy-s7-edge-32gb-pink-gold','Shop Now'),
            //builder.CardAction.imBack(session,"Back")
          ]),


  ]);
  session.endDialog(msg);
}).triggerAction({matches:'Android categories'});

// Top sellers Apple iphones crausel:
bot.dialog('apple categories',function(session){
var msg= new builder.Message(session)
  .attachmentLayout(builder.AttachmentLayout.carousel)
  .attachments([
    new builder.HeroCard(session)
      .title("Apple iPhone 7")
      .subtitle("32GB Black")
      //.text("Rs. 49,999")
      .images([
        builder.CardImage.create(session,"https://www.gizmofashion.com/media/bot/a/apple_iphone_7_black_-_gizmofashion_-_main_image.jpg")
      ])
      .buttons([
       builder.CardAction.openUrl(session,'http://www.gizmofashion.com/apple-iphone-7-32gb-black','Shop Now'),
        //builder.CardAction.imBack(session,"Back")
      ]),
    new builder.HeroCard(session)
      .title("Apple iPhone 7 Plus")
      .subtitle("128GB Silver")
      //.text("Rs. 79,000")
.images([
        builder.CardImage.create(session,'https://www.gizmofashion.com/media/bot/a/apple_iphone_7_plus_silver_side_-_gizmofashion_2.jpg')
    ])
      .buttons([
         builder.CardAction.openUrl(session,'http://www.gizmofashion.com/apple-iphone-7-plus-128gb-silver','Shop Now'),
        //  builder.CardAction.imBack(session,"select:100","Select")
      ]),
      new builder.HeroCard(session)
        .title("Apple iPhone 7")
        .subtitle("32GB Rose Gold")
        //.text("Rs. 49,999")
        .images([
          builder.CardImage.create(session,"https://www.gizmofashion.com/media/bot/a/apple_iphone_7_plus_silver_side_-_gizmofashion_2.jpg")
        ])
        .buttons([
         builder.CardAction.openUrl(session,'http://www.gizmofashion.com/apple-iphone-7-32gb-rose-gold','Shop Now'),
          //builder.CardAction.imBack(session,"Back")
        ]),
    new builder.HeroCard(session)
      .title("Apple iPhone 7 Plus")
      .subtitle("256GB Jet Black")
      //.text("Rs.91,500")
      .images([
        builder.CardImage.create(session,"https://www.gizmofashion.com/media/bot/a/apple_iphone_7_plus_jet_black_side_-_gizmofashion_1.jpg")
      ])
      .buttons([
       builder.CardAction.openUrl(session,'http://www.gizmofashion.com/apple-iphone-7-plus-256gb-jet-black','Shop Now'),
        //builder.CardAction.imBack(session,"Back")
      ]),

      new builder.HeroCard(session)
      .title("Apple iPhone 7 Plus")
      .subtitle("128GB Red")
      //.text("Rs. 80,000")
        .images([
          builder.CardImage.create(session,"https://www.gizmofashion.com/media/bot/a/apple_iphone_7_plus_red_side_-_gizmofashion.jpg")
        ])
        .buttons([
         builder.CardAction.openUrl(session,'http://www.gizmofashion.com/apple-iphone-7-plus-128gb-red','Shop Now'),
          //builder.CardAction.imBack(session,"Back")
        ]),
  ]);
  session.endDialog(msg);
}).triggerAction({matches:'Apple categories'});
